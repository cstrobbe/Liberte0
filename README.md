# Liberté 0 / Freedom #0

## Français

Ceci est le "repo" temporaire du site web statique de l'association Liberté 0.

Vous pouvez visiter le site web statique à l'adresse http://cstrobbe.gitlab.io/Liberte0/.

## English

Temporary repository of static code for the website of Liberté 0 / Freedom #0 (formerly hosted at liberte0.org).

The English version of the temporary website can be accessed at http://cstrobbe.gitlab.io/Liberte0/en/.

The content of this website is available under the Creative Commons [CC BY-SA 4.0](legal.html).

